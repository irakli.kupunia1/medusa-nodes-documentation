=========================================
Medusa Nodes Documentation GitLab Repo
=========================================

Medusa Nodes is an addon for Blender, leveraging the power of `Geometry Nodes <https://docs.blender.org/manual/en/latest/modeling/geometry_nodes/index.html>`_ to facilitate procedural hair creation. With a suite of custom-made Geometry Nodes deformers, Medusa Nodes streamlines the process of generation and manipulation of hair in Blender.

Feedback and Support
--------------------

We value the feedback from the community to improve Medusa Nodes. Feel free to report bugs, suggest features, or discuss any issues you encounter on the `issue tracker <https://github.com/.../issues>`_.

Notes
-----

- Please be aware that Medusa Nodes is not yet production-ready and may have some bugs.
- For those interested in the broader scope of procedural hair creation in Blender, the `Blender Studio Blog <https://studio.blender.org/blog/procedural-hair-nodes/>`_ provides a deep dive into procedural hair node assets and the ecosystem of node-groups aimed at procedural hair grooming.

Development
-----------

This project started in August 2021 and is an ongoing endeavor to provide a robust solution for procedural hair creation within Blender. While the addon has evolved since its inception, it's still in beta(!) and under active development.

Acknowledgements
----------------

Special thanks to the developer Lukman Boulbayam for his work in addon's UI scripting department.
