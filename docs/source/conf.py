# Configuration file for the Sphinx documentation builder.

# -- Project information

project = 'Medusa Nodes'
copyright = '2024, Medusa Nodes'
author = 'Irakli Kupunia'

#release = '0.1'
version = '1.1.1-beta'
# The full version, including alpha/beta/rc tags.
release = version
# -- General configuration

extensions = [
    'sphinx.ext.duration',
    'sphinx.ext.doctest',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.intersphinx',
    'sphinx.ext.extlinks',
    'sphinx_rtd_theme',
    ]

intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}
intersphinx_disabled_domains = ['std']

templates_path = ['_templates']

# -- Options for HTML output

html_theme = 'sphinx_rtd_theme'

# -- Options for EPUB output
epub_show_urls = 'footnote'


#html_static_path = ['_static']
html_logo = 'images/medusa_node_logo_animation_003.gif'
html_theme_options = {
    'logo_only': False,
    'display_version': True,
}
#favicon for the google search entry icons
html_favicon = "images/medusa_nodes_favicon.ico"

html_static_path = ['_static']
html_css_files = ['custom.css']
