.. figure:: images/hero1024.jpg
   :width: 100%
   :align: center
   :alt: hero banner
   

==============================

Medusa Nodes Documentation
==============================

.. meta::
   :description: documentation website for Medusa Nodes addon


**Medusa Nodes** - an addon for procedural hair creation, based entirely on
`Geometry Nodes <https://docs.blender.org/manual/en/latest/modeling/geometry_nodes/index.html>`_
to facilitate procedural hair creation. The addon provides ability to generate and manipulate different hair styles inside the Blender.

Medusa Nodes features custom-made Geometry Nodes :doc:`node groups <nodes>`, which are purposely made to achieve realistic deformations and visualization of hair in 3D.

The addon is streamlined by the help of addons own hierarchy panel, 
where one can easily view and edit the parameters of all the different nodes: :term:`generator`,  :doc:`deformers <nodes>` and :doc:`nodes/visualizer`.

Check out the :doc:`about` section for further information about this project.

Getting Started
---------------

- **Installation:** check out the quick :doc:`installation <installation>` guide.
- **Tutorials:** two-part introductory tutorial series are available to help you get familiar with Medusa Nodes:

.. <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=Lm0G3H38eKc4ON_P&amp;list=PLUMDBJubYLVxjw_d-9h0zDguw0FTbS4LU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

.. raw:: html


   <iframe

      src="https://www.youtube.com/embed/videoseries?si=Lm0G3H38eKc4ON_P&amp;list=PLUMDBJubYLVxjw_d-9h0zDguw0FTbS4LU"
      title="YouTube video player"
      frameborder="0"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
      referrerpolicy="strict-origin-when-cross-origin"
      allowfullscreen></iframe>

   </iframe>



- **Community Discussions:** get to know with the community on `Blender Artists <https://blenderartists.org/t/hair-medusa-nodes>`_ for insights, support, and to share your experiences with Medusa Nodes.



.. note::

   The addon as well as it's documentation are subjects of continuous development.

.. warning:: 

   Keep in mind, that the Medusa Nodes addon is in it's **beta** development phase, may exhibit some bugs and **not yet production-ready**!



Feedback and Support
--------------------

Community feedback is greatly appreciated. Feel free to report bugs, suggest features, or discuss any issues on the `issue tracker <https://github.com/.../issues>`_.

Contents
--------

.. toctree::
   :maxdepth: 3

   about
   release_notes
   installation
   introduction
   nodes
   


   
