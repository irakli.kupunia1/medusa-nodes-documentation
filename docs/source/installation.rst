.. _installation:

Installation
==============



The **Medusa Nodes** addon can be installed just as every other Blender addon. No additional steps are required:

1. Go to *Edit > Preferences > Add-ons*.
2. Click on *Install* button in the upper right corner of the Preferences window.

.. figure:: images/installation_001.jpg
   :width: 400
   :figwidth: 100%
   :align: left

3. Navigate to the directory where the *medusaNodes-vX.X.X.zip* file is located and select it, then click on *Install Add-on* button in the lower right corner of the file browser window.

.. figure:: images/installation_002.jpg
   :width: 400
   :figwidth: 100%
   :align: left

4. After the addon appears in the *Preferences* window, simply enable it by cheking on the box to the left side of the addon's name.
   
.. figure:: images/installation_003.jpg
   :width: 400
   :figwidth: 100%
   :align: left
