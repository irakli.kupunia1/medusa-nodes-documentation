.. _introduction:

Introduction
=============


Workflow
**********

*Medusa Nodes* utilizes a workflow similar to many other industry-standard procedural hair creation toolsets, but it also incorporates unique Blender-esque features made possible by Blender's powerful Geometry Nodes system.

To be able to understand the workflow, first we need to understand which types of objects *Medusa Nodes* uses.

-----------------------

Object Types
------------------

There are 3 distinct object types relevant to the *Medusa Nodes* procedural workflow:

1. **Hair**
   
   - *Hair* object is the actual hair or fur of a 3D character, with individual hair follicles represented as splines.
   - *Hair* object receives all the deformations from different deformers and is intended for rendering.

2. **Guide**
   
   - *Guide* object is similar to *Hair* object internally, but it serves as parent for splines (individual hair follicles) in the *Hair* object.
   - *Guides* deform the splines using the Guide deformer, by doing so they define the flow and direction of the hair.
   - In the context of Blender Particle Hair: the *Guide* is the parent particle, while the *Hair* is the interpolated child particle.

3. **Emitter**
   
   - *Emitter* is the mesh geometry used by *Hair* object to generate hair strands.
   - It's essential for *Emitter* to have a proper UVMap.

.. figure:: images/structure.JPG
   :width: 400
   :figwidth: 100%
   :align: center


---------------------------------------

.. "Guides" ro "Guideless" Workflow
---------------------------------

.. Whenever you start working on your new hair style and hit the button **New Groom**, the addon will create a *Hair* object. There are two different workflows available, which we will refer to as **Guides Workflow** and **Guideless Workflow**.

.. **Guides Workflow**:
~~~~~~~~~~~~~~~~~~~~

.. First, you can add the *Guide* deformer, which controls the overall shape of the hairstyle. After adding the *Guide* deformer, you can include all other deformers.

.. *Pros*:
.. - Suitable for both portrait grooms and full-body grooms.
.. - Offers the ability to quickly control a higher number of splines within the *Hair* object.

.. *Cons*:
.. - Slightly more complex to use.

.. **Guideless Workflow**:
~~~~~~~~~~~~~~~~~~~~~~~~~

.. In this workflow, you can add deformers like *Noise*, *Curl*, or *Children* to the groom without including the *Guide* deformer.

.. *Pros*:

.. *Cons*:
.. - Slightly more complex to use.



     
