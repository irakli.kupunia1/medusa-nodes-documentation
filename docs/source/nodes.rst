.. _nodes:

.. figure:: /images/medusa_nodes_nodes.jpg
   :width: 800px
   :figwidth: 100%
   :align: center
   :alt: Add Deformers Menu
  

   |nbspc|
  
Nodes
=====


**Medusa Nodes** uses custom-made nodes (node groups) built inside Geometry Nodes.

There are two types of node groups:

Main Nodes
----------

.. container:: dual-content

   .. container:: left

      Main nodes affect the hair and guide objects. There are four types of main nodes:

      - **Initials**:
         Nodes that initially come with every new hair or guide object. They can't be removed.
      
      - **Deformers**:
         Procedural modeling nodes that deform or shape the splines in a realistic manner.
      
      - **Utility**:
         Responsible for some of the more technical procedural spline editing.
      
      - **Simulation**:
         Nodes that utilize different solving algorithms accompanied with caching.

   .. container:: right

      .. figure:: /images/node_icons/add_deformers_menu_002.png
         :width: 300px
         :figwidth: 100%
         :align: center
         :alt: Add Deformer Menu

         "Add Deformer" Menu

Initials
--------

.. list-table::
   :widths: 25 60

   * - |generator_icon| |nbspc| :doc:`nodes/generator`  
     - Generates splines (hair follicles) on the mesh object.
   * - |visualizer_icon| |nbspc| :doc:`nodes/visualizer`
     - Visualizes splines by adding thickness and material shading.

.. toctree::
   :hidden:
   :maxdepth: 1

   nodes/generator
   nodes/visualizer


Deformers
---------

.. list-table::
   :widths: 25 80
   

   * - |guide_icon| |nbspc| :doc:`nodes/guide`
     - Shapes the hair with the help of the **Guide object**.
   * - |clump_icon| |nbspc| :doc:`nodes/clump`
     - Creates various kinds of clumps, supports nested-clumping.
   * - |braid_icon| |nbspc| :doc:`nodes/braid`
     - Adds regular and fishbone braids.
   * - |noise_icon| |nbspc| :doc:`nodes/noise`
     - Adds noise-based deformation.
   * - |curl_icon| |nbspc| :doc:`nodes/curl`
     - Adds curls to hair.
   * - |trim_icon| |nbspc| :doc:`nodes/trim`
     - Trims / cuts the hair.

.. toctree::
   :hidden:
   :maxdepth: 1

   nodes/guide
   nodes/clump
   nodes/braid
   nodes/noise
   nodes/curl
   nodes/trim

Utility
-------

.. list-table::
   :widths: 25 80

   * - |curves_to_splines_icon| |nbspc| :doc:`nodes/curves_to_splines`
     - Gives the ability to draw and edit Blender's regular curves objects on Medusa Nodes **Hair object**.
   * - |mirror_icon| |nbspc| :doc:`nodes/mirror`
     - Mirrors the guide and the hair splines along multiple axes.
   * - |scale_icon| |nbspc| :doc:`nodes/scale`
     - Scales the guide and the hair splines by the custom factor.
   * - |resample_hair_icon| |nbspc| :doc:`nodes/resample_hair`
     - Resamples and redistributes the control points in the splines.
   * - |children_icon| |nbspc| :doc:`nodes/children`
     - Randomly adds simple copies of already existing hair follicles.

.. toctree::
   :hidden:
   :maxdepth: 1

   nodes/curves_to_splines
   nodes/mirror
   nodes/scale
   nodes/resample_hair
   nodes/children

Simulation
-----------

.. list-table::
   :widths: 25 80

   * - |cache_icon| |nbspc| :doc:`nodes/cache`
     - Caches the deformation produced by the simulation or animation, making the playback faster.
   * - |dynamics_icon| |nbspc| :doc:`nodes/dynamics`
     - Simulates guide and hair splines using custom dynamics solver.

.. toctree::
   :hidden:
   :maxdepth: 1

   nodes/cache
   nodes/dynamics

Mask Groups
-------------

.. container:: dual-content

   .. container:: left

      Mask Groups can alter different parameters of the Main nodes, mainly by producing black and white values.
      
      They are not always necessary but can help modulate the effect produced by a Main node.

   .. container:: right

      .. figure:: /images/node_icons/mask_groups/mask_groups.png
         :width: 300px
         :figwidth: 100%
         :align: center
         :alt: Mask Group Options menu's screenshot

         "Mask Group Options" menu.

.. list-table::
   :widths: 25 80

   * - .. centered:: |texture_mask_group_icon| |nbspc| :doc:`nodes/maskGroups/texture`
     - Uses painted textures as an influence mask.
   * - .. centered:: |random_mask_group_icon| |nbspc| :doc:`nodes/maskGroups/random`
     - Uses a random function as an influence mask.
   * - .. centered:: |percent_mask_group_icon| |nbspc| :doc:`nodes/maskGroups/percent`
     - Uses a percent function as an influence mask.
   * - .. centered:: |noise_mask_group_icon| |nbspc| :doc:`nodes/maskGroups/noise`
     - Uses a noise function as an influence mask.
   * - .. centered:: |curve_mask_group_icon| |nbspc| :doc:`nodes/maskGroups/curve`
     - Uses a Float Curve as an influence mask.
   * - .. centered:: |ramp_mask_group_icon| |nbspc| :doc:`nodes/maskGroups/ramp`
     - Uses a Color Ramp as an influence mask.

.. toctree::
   :hidden:
   :maxdepth: 1

   nodes/maskGroups/texture
   nodes/maskGroups/random
   nodes/maskGroups/percent
   nodes/maskGroups/noise
   nodes/maskGroups/curve
   nodes/maskGroups/ramp

.. |nbspc| unicode:: U+00A0 

.. |generator_icon| image::  ./images/node_icons/hair/generator.png
   :height: 25px
   :alt: Generator Icon.

.. |visualizer_icon| image::  ./images/node_icons/hair/visualizer.png
   :height: 25px
   :alt: Visualizer Icon.

.. |guide_icon| image::  ./images/node_icons/hair/guide.png
   :height: 25px
   :alt: Guide Icon.

.. |clump_icon| image::  ./images/node_icons/hair/clump.png
   :height: 25px
   :alt: Clump Icon.

.. |braid_icon| image::  ./images/node_icons/hair/braid.png
   :height: 25px
   :alt: Braid Icon.

.. |noise_icon| image::  ./images/node_icons/hair/noise.png
   :height: 25px
   :alt: Noise Icon.

.. |curl_icon| image::  ./images/node_icons/hair/curl.png
   :height: 25px
   :alt: Curl Icon.

.. |trim_icon| image::  ./images/node_icons/hair/trim.png
   :height: 25px
   :alt: Trim Icon.

.. |curves_to_splines_icon| image::  ./images/node_icons/hair/curves_to_splines.png
   :height: 25px
   :alt: Curves to Splines Icon.

.. |mirror_icon| image::  ./images/node_icons/hair/mirror.png
   :height: 25px
   :alt: Mirror Icon.

.. |scale_icon| image::  ./images/node_icons/hair/scale.png
   :height: 25px
   :alt: Scale Icon.

.. |resample_hair_icon| image::  ./images/node_icons/hair/resample_hair.png
   :height: 25px
   :alt: Resample Hair Icon.

.. |children_icon| image::  ./images/node_icons/hair/children.png
   :height: 25px
   :alt: Children Icon.

.. |cache_icon| image::  ./images/node_icons/hair/cache.png
   :height: 25px
   :alt: Cache Icon.

.. |dynamics_icon| image::  ./images/node_icons/hair/dynamics.png
   :height: 25px
   :alt: Dynamics Icon.

.. |texture_mask_group_icon| image::  ./images/node_icons/mask_groups/texture.png
   :height: 25px
   :alt: Texture Mask Icon.

.. |random_mask_group_icon| image::  ./images/node_icons/mask_groups/random.png
   :height: 25px
   :alt: Random Mask Icon.
.. |percent_mask_group_icon| image::  ./images/node_icons/mask_groups/percent.png
   :height: 25px
   :alt: Percent Mask Icon.

.. |noise_mask_group_icon| image::  ./images/node_icons/mask_groups/noise.png
   :height: 25px
   :alt: Noise Mask Icon.

.. |curve_mask_group_icon| image::  ./images/node_icons/mask_groups/curve.png
   :height: 25px
   :alt: Curve Mask Icon.

.. |ramp_mask_group_icon| image::  ./images/node_icons/mask_groups/ramp.png
   :height: 25px
   :alt: Ramp Mask Icon.

